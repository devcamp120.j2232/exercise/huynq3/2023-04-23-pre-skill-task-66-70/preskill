package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.models.District;
import com.devcamp.task62jpaprovincerelationship.models.Ward;
import com.devcamp.task62jpaprovincerelationship.repository.DistrictRepository;

@Service
public class DistrictService {
    
    @Autowired
    DistrictRepository districtRepository;
    
    public ArrayList<District> getDistrictList(){
        ArrayList<District> listDistricts = new ArrayList<>();
        districtRepository.findAll().forEach(listDistricts::add);
        return listDistricts;
    }
   
    public Set<Ward> getWardByDistrictId(int id){
        District thisDistrict = districtRepository.findById(id);
        if (thisDistrict != null){
            return thisDistrict.getWards();
        }else{
            return null;
        }
    }
        

}