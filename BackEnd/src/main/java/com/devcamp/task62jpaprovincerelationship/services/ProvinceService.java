package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.models.District;
import com.devcamp.task62jpaprovincerelationship.models.Province;
import com.devcamp.task62jpaprovincerelationship.repository.ProvinceRepository;

@Service
public class ProvinceService {
  @Autowired
  ProvinceRepository provinceRepository;

  public List<String> getAllProvinces() {
    List<Province> provinceList = provinceRepository.findAll();
    List<String> listall = new ArrayList<>();
    listall = provinceList.stream().map(Province::getName).collect(Collectors.toList());
    return listall;
  }

  public ArrayList<Province> getProvinceListInfo() {

    ArrayList<Province> listProvinces = new ArrayList<>();
    provinceRepository.findAll().forEach(listProvinces::add);
    return listProvinces;
  }
//Lay District theo Province Id
  public Set<District> getDistrictByProvinceId(int id) {
    Province thisProvince = provinceRepository.findById(id);
    if (thisProvince != null) {
      return thisProvince.getDistricts();
    } else {
      return null;
    }
  }

}