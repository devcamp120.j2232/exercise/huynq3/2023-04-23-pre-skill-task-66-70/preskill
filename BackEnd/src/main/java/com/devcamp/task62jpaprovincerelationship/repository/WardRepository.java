package com.devcamp.task62jpaprovincerelationship.repository;

import com.devcamp.task62jpaprovincerelationship.models.Ward;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WardRepository  extends JpaRepository<Ward, Integer>{
    
}
