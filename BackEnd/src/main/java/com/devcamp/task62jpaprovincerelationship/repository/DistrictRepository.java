package com.devcamp.task62jpaprovincerelationship.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task62jpaprovincerelationship.models.District;

public interface DistrictRepository extends JpaRepository <District, Integer> {
    District findById(int id);
}
