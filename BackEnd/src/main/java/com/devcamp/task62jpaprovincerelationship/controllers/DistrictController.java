package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.models.Ward;
import com.devcamp.task62jpaprovincerelationship.services.DistrictService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class DistrictController {
    @Autowired
    private DistrictService districtService;

    @GetMapping("/ward-in-district")
    public ResponseEntity<Set<Ward>> getWardByDistrictId(@RequestParam(name = "id") int id) {
        try {
            Set<Ward> listWards = districtService.getWardByDistrictId(id);
            if (listWards != null) {
                return new ResponseEntity<>(listWards, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
