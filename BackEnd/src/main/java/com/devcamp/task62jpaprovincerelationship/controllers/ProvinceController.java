package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageRequestDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.devcamp.task62jpaprovincerelationship.models.District;
import com.devcamp.task62jpaprovincerelationship.models.Province;
import com.devcamp.task62jpaprovincerelationship.repository.ProvinceRepository;
import com.devcamp.task62jpaprovincerelationship.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    private ProvinceService provinceService;
    @GetMapping("/provinces")
    public ResponseEntity<List<String>> getProvinces(){
        try {
            return new ResponseEntity<>(provinceService.getAllProvinces(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all-provinces-info")
    public ResponseEntity <List<Province>> getAllProvincesByService() {
        try {
            return new ResponseEntity<>(provinceService.getProvinceListInfo(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);    
        }
    }
 @GetMapping("/province5")
    public ResponseEntity<List<Province>> getFiveProvinces(
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "1") String size){
        try {
            org.springframework.data.domain.Pageable pageWithFiveProvince = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Province> list = new ArrayList<Province>();
            provinceRepository.findAll(pageWithFiveProvince).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }
   
    @GetMapping("/district-in-province")
    public ResponseEntity<Set<District>> getDistrictByProvinceId(@RequestParam(name="id") int id){
        try {
            Set<District> listDistricts = provinceService.getDistrictByProvinceId(id);
            if (listDistricts != null){
                return new ResponseEntity<>(listDistricts, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
